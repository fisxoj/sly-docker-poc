(defpackage lack.middleware.slynk
  (:use #:cl)
  (:export #:*lack-middleware-slynk*))

(in-package #:lack.middleware.slynk)

(defparameter *lack-middleware-slynk*
  (lambda (app &key (port slynk::default-server-port))
    (load (make-pathname :name "slynk-loader"
    			 :type "lisp"
    			 :defaults (asdf:component-pathname (asdf:find-system :slynk))))
    (uiop:symbol-call :slynk-loader :init
    		      :delete nil
    		      :reload nil)
    (format t "~&Starting slynk server~%")
    (slynk:create-server :port port
			 :style :spawn
			 :interface "0.0.0.0"
			 :dont-close t)

    (format t "~&...started~%")
    app))
