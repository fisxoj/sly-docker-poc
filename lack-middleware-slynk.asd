(defsystem lack-middleware-slynk
  :author "Matt Novenstern"
  :license "LLGPLv3+"
  :depends-on ("lack"
	       "slynk")
  :components ((:file "src/lack-middleware-slynk")))
