LISP=sbcl --noninteractive
IMAGE_NAME=lack-middleware-slynk

build: Dockerfile
	docker build -t $(IMAGE_NAME) .

run:
	docker run --rm -it -p 4008:4008 -p 5000:5000 $(IMAGE_NAME)

clean:
	docker image rm $(IMAGE_NAME)
