(ql:quickload '("clack" "slynk" "lack-middleware-slynk"))

(defparameter *inner-app*
  (lambda (env)
    (declare (ignore env))
    '(200 (:content-type "text/plain") ("Hello, World"))))

(format t "~&Building app~%")
(defparameter *app*
  (lack:builder
   :accesslog
   ;; (:slynk :port 4008)
   *inner-app*))
(format t "~&... done~%")

(format t "~&Starting slynk~%")
(setf slynk:*log-events* t)
(slynk:create-server :port 4008
		     ;; :style :spawn
		     :interface "0.0.0.0"
		     :dont-close t)
(format t "~&... done~%")

(format t "~&Starting webapp server~%")
(clack:clackup *app*
	       :port 5000
	       :use-thread nil)
(format t "~&... done~%")
