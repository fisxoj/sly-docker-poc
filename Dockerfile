FROM parentheticalenterprises/sbcl-quicklisp

RUN apt-get install -y -qq git

RUN sbcl --non-interactive --eval "(ql:quickload \"lack\")"

RUN git clone https://github.com/joaotavora/sly.git /quicklisp/local-projects/sly

RUN git clone https://github.com/joaotavora/sly-named-readtables.git /quicklisp/local-projects/sly-named-readtables

RUN git clone https://github.com/joaotavora/sly-macrostep.git /quicklisp/local-projects/sly-macrostep

RUN git clone https://github.com/joaotavora/sly-company.git /quicklisp/local-projects/sly-company

RUN git clone https://github.com/joaotavora/sly-repl-ansi-color.git /quicklisp/local-projects/sly-repl-ansi-color

RUN git clone https://github.com/joaotavora/sly-quicklisp.git /quicklisp/local-projects/sly-quicklisp

RUN sbcl --noninteractive --eval "(ql:quickload '(slynk clack))"

COPY example/app.lisp /app.lisp

RUN mkdir -p /quicklisp/local-projects/lack-middleware-slynk/src

COPY lack-middleware-slynk.asd /quicklisp/local-projects/lack-middleware-slynk/

COPY ./src/lack-middleware-slynk.lisp /quicklisp/local-projects/lack-middleware-slynk/src/

EXPOSE 4008 5000

CMD ["sbcl", "--non-interactive", "--load", "/app.lisp"]
